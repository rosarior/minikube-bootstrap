#!/bin/sh

wget https://github.com/ahmetb/kubectx/releases/download/v0.9.1/kubectx_v0.9.1_linux_x86_64.tar.gz -O - | tar -z --directory=/tmp --extract kubectx
sudo cp /tmp/kubectx /usr/local/bin/
sudo chmod +x /usr/local/bin/kubectx

wget https://github.com/ahmetb/kubectx/releases/download/v0.9.1/kubens_v0.9.1_linux_x86_64.tar.gz -O - | tar -z --directory=/tmp --extract kubens
sudo cp /tmp/kubens /usr/local/bin/
sudo chmod +x /usr/local/bin/kubens

curl -L "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl" --output /tmp/kubectl
sudo cp /tmp/kubectl /usr/local/bin/
sudo chmod +x /usr/local/bin/kubectl
kubectl completion bash > /tmp/kubectl
sudo cp /tmp/kubectl /usr/share/bash-completion/completions/

wget https://get.helm.sh/helm-v3.3.0-linux-amd64.tar.gz -O - | tar -z --directory=/tmp --extract linux-amd64/helm
sudo cp /tmp/linux-amd64/helm /usr/local/bin/
sudo chmod +x /usr/local/bin/helm
helm completion bash > /tmp/helm
sudo cp /tmp/helm /usr/share/bash-completion/completions/

wget -c https://github.com/bcicen/ctop/releases/download/v0.7.3/ctop-0.7.3-linux-amd64 -O /tmp/ctop
sudo cp /tmp/ctop /usr/local/bin/
sudo chmod +x /usr/local/bin/ctop

